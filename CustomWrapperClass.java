import java.util.Scanner;

public class CustomWrapperClass {
    public static void main(String []args){
        System.out.println("Enter a value for autoboxing");
        Scanner scn= new Scanner(System.in);
        int x=scn.nextInt();
        Integ value = new Integ(x);
        System.out.println("Converted string "+value.getvalue());
    }

}

class Integ{
    private int val;
    Integ(int x){
        this.val=x;
    }
    public int getvalue(){
        return this.val;
    }

}
